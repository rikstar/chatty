var _ = require('lodash');
var inflect = require('inflect');

// /iou <thing> [xN] @user     declare that you owe someone something
// /paid <thing> [xN] @user    declare that you paid someone something (x N, if specified)
// /owe [@user]                see what you owe (filtering by a specific user if specified)
// NYI
// /owed [@user]               see what you are owed (filtering by a specific user if specified)
module.exports = function (commander, logger) {

  commander.script({
    help: 'A set of commands for managing IOUs'
  });

  commander.command({
    name: 'iou',
    args: '<thing> [xN] @mention',
    help: 'You owe a user (N of) some thing',
    action: function (event, response) {
      var match = /^(\w+)\s+(?:x\s?(\d+)\s+)?@(\w+)\b/.exec(event.input);
      if (!match) {
        return response.confused();
      }
      var ower = event.from.mention_name;
      var owerName = event.from.name;
      var thing = match[1].toLowerCase();
      var count = +(match[2] || '1');
      var owee = match[3];
      var oweeName = displayName(event, owee);
      var key = iouKey(ower, owee);
      event.store.gget(key).then(function (entry) {
        entry = entry || {things: {}, oweeName: oweeName};
        entry.things[thing] = (entry.things[thing] || 0) + count;
        event.store.gset(key, entry).then(function () {
          var total = entry.things[thing];
          var msg = 'Ok, ' + owerName + ' owes ' + oweeName + ' ' + total + ' ' +
                    (total > 1 ? inflect.pluralize(thing) : thing);
          response.send(msg);
        });
      });
    }
  });

  commander.command({
    name: 'paid',
    args: '<thing> [xN] @mention',
    help: 'You paid a user (N of) some thing',
    action: function (event, response) {
      var match = /^(\w+)\s+(?:x\s?(\d+)\s+)?@(\w+)\b/.exec(event.input);
      if (!match) {
        return response.confused();
      }
      var ower = event.from.mention_name;
      var owerName = event.from.name;
      var thing = match[1].toLowerCase();
      var count = +(match[2] || '1');
      var owee = match[3];
      var oweeName = displayName(event, owee);
      var key = iouKey(ower, owee);
      event.store.gget(key).then(function (entry) {
        var noMatchMsg = 'You don\'t owe ' + oweeName + ' ' + thing;
        var msg;
        if (!entry) {
          return response.send(noMatchMsg);
        }
        if (!entry.things[thing] || entry.things[thing] < count) {
          msg = 'You don\'t owe ' + oweeName + ' that many ' + inflect.pluralize(thing);
          response.send(msg);
        } else {
          entry.things[thing] -= count;
          if (entry.things[thing] === 0) {
            delete entry.things[thing];
          }
          var paidMsg = 'Ok, ' + owerName + ' paid ' + oweeName + ' ' +
                        count + ' ' + (count > 1 ? inflect.pluralize(thing) : thing);
          if (Object.keys(entry.things).length > 0) {
            event.store.gset(key, entry).then(function () {
              response.send(paidMsg);
            });
          } else {
            event.store.gdel(key).then(function () {
              response.send(paidMsg);
            });
          }
        }
      });
    }
  });

  commander.command({
    name: 'owe',
    args: '[@mention]',
    opts: {format: 'html'},
    help: 'Checks what you owe to whom, filtered by mention name',
    action: function (event, response) {
      var match = /^@(\w+)\b/.exec(event.input);
      var ower = event.from.mention_name;
      var owerName = event.from.name;
      var owee = match && match[1];
      var oweeName = owee ? displayName(event, owee) : null;
      function displayOwed(entry, oweeName) {
        var msg = owerName + ' owes ' + oweeName;
        msg += entry ? ': ' + displayThings(entry.things) : ' nothing';
        return msg;
      }
      if (owee) {
        var oweeName = owee ? displayName(event, owee) : null;
        var key = iouKey(ower, owee);
        return event.store.gget(key).then(function (entry) {
          var msg = displayOwed(entry, oweeName);
          response.send(msg);
        });
      } else {
        return event.store.gall(ower).then(function (owees) {
          var zeroMsg = 'You have no oustanding debts.  Way to go!';
          if (owees) {
            var msg = [];
            Object.keys(owees).sort().map(function (owee) {
              owee = owee.slice(owee.lastIndexOf(':') + 1);
              var entry = owees[owee];
              var line = displayOwed(entry, entry.oweeName);
              msg.push(line);
            });
            if (msg.length === 0) {
              response.send(zeroMsg);
            } else {
              response.send(msg.join('<br>\n'));
            }
          } else {
            response.send(zeroMsg);
          }
        });
      }
    }
  });

  function iouKey(from, to) {
    return from + ':' + to;
  }

  function displayName(event, mentionName) {
    var mention = _.find(event.mentions, function (entry) {
      return entry.mention_name === mentionName;
    });
    return mention ? mention.name : mentionName;
  }

  function displayThings(things) {
    var msg = '';
    var first = true
    Object.keys(things).sort().forEach(function (thing) {
      var count = things[thing];
      msg += (first ? '' : ', ') + count + ' ' + (count > 1 ? inflect.pluralize(thing) : thing);
      first = false;
    });
    return msg;
  }

};
